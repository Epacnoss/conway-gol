use crate::dirs::{get_file_checked, DirLocation, DEFAULT_CONFIG};
use crate::gol::Cell;
use crate::rule_sets::*;
use ron::de::from_str;
use serde::Deserialize;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum StartType {
    Blank,
    Noise(f64),
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Algorithm {
    Corruptor,
    Labyrinth,
    Vanilla,
}

#[derive(Deserialize)]
struct ReadInData {
    start: String,
    chance: Option<f64>,
    start_ups: u64,
    rules: Option<String>,
    start_lines: bool,
    start_running: bool,
    alive: (f32, f32, f32, f32),
    dead: (f32, f32, f32, f32),
    just_died: (f32, f32, f32, f32),
    lines: (f32, f32, f32, f32),
    whs: (usize, usize, usize),
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Config {
    pub start: StartType,
    pub start_ups: u64,
    pub algorithm: Algorithm,
    pub start_lines: bool,
    pub start_running: bool,
    pub alive: (f32, f32, f32, f32),
    pub dead: (f32, f32, f32, f32),
    pub just_died: (f32, f32, f32, f32),
    pub lines: (f32, f32, f32, f32),
    pub width: usize,
    pub height: usize,
    pub scale: usize,
}

impl Config {
    pub fn default() -> Self {
        Config {
            start: StartType::Noise(0.35),
            // start: StartType:: Blank,
            start_ups: 2,
            algorithm: Algorithm::Vanilla,
            start_lines: true,
            start_running: false,
            alive: (0.0, 0.0, 0.0, 1.0),
            dead: (1.0, 1.0, 1.0, 1.0),
            just_died: (1.0, 0.0, 0.0, 1.0),
            lines: (0.5, 0.5, 0.5, 1.0),
            width: 500,
            height: 500,
            scale: 2,
        }
    }

    #[allow(dead_code, clippy::too_many_arguments)]
    pub fn custom(
        start: StartType,
        start_ups: u64,
        algorithm: Algorithm,
        start_lines: bool,
        start_running: bool,
        alive: (f32, f32, f32, f32),
        dead: (f32, f32, f32, f32),
        just_died: (f32, f32, f32, f32),
        lines: (f32, f32, f32, f32),
        whs: (usize, usize, usize),
    ) -> Self {
        Config {
            start,
            start_ups,
            algorithm,
            start_lines,
            start_running,
            alive,
            dead,
            just_died,
            lines,
            width: whs.0,
            height: whs.1,
            scale: whs.2,
        }
    }

    pub fn new(file: String) -> Self {
        let str = get_file_checked(DirLocation::Config, file)
            .0
            .unwrap_or_else(|_| DEFAULT_CONFIG.to_string());

        match from_str(str.as_str()) {
            Ok(worked) => {
                let w: ReadInData = worked;

                let chance = w.chance.unwrap_or(1.0);
                let start = match w.start.as_str() {
                    "Blank" => StartType::Blank,
                    _ => StartType::Noise(chance),
                };

                let algorithm = match w.rules.unwrap_or_else(|| String::from("Vanilla")).as_str() {
                    "Corruptor" => Algorithm::Corruptor,
                    "Labyrinth" => Algorithm::Labyrinth,
                    _ => Algorithm::Vanilla,
                };

                Config {
                    start,
                    start_ups: w.start_ups,
                    algorithm,
                    start_lines: w.start_lines,
                    start_running: w.start_running,
                    alive: w.alive,
                    just_died: w.just_died,
                    dead: w.dead,
                    lines: w.lines,
                    width: w.whs.0,
                    height: w.whs.1,
                    scale: w.whs.2,
                }
            }
            Err(e) => {
                eprintln!("Error parsing conf:\n {:?}", e);
                Self::default()
            }
        }
    }

    pub fn get_algo(&self, n: i32, b: Cell) -> Cell {
        let a = match self.algorithm {
            Algorithm::Corruptor => corruptor,
            Algorithm::Labyrinth => labyrinth,
            Algorithm::Vanilla => vanilla,
        };
        a(n, b)
    }
    #[allow(dead_code)]
    pub fn get_algo_bool_edition(&self, n: i32, b: bool) -> bool {
        let c;
        if b {
            c = Cell::Alive;
        } else {
            c = Cell::Dead;
        }

        let res = self.get_algo(n, c);
        Cell::get_bool_state(res)
    }
}

#[cfg(test)]
mod read_in_tests {
    use super::*;
    use crate::dirs::dirs_test::get_bogus;

    #[test]
    fn get_algo() {
        let conf = Config::default();
        assert!(!conf.get_algo_bool_edition(1, false));
        assert!(!conf.get_algo_bool_edition(1, true));
        assert!(!conf.get_algo_bool_edition(2, false));

        assert!(conf.get_algo_bool_edition(2, true));
        assert!(conf.get_algo_bool_edition(3, true));
        assert!(conf.get_algo_bool_edition(3, false));

        for i in 4..8 {
            assert!(!conf.get_algo_bool_edition(i, true));
            assert!(!conf.get_algo_bool_edition(i, false));
        }
    }

    #[test]
    fn check_bad_new() {
        assert_eq!(Config::default(), Config::new(get_bogus()));
    }
}
