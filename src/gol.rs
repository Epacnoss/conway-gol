use crate::read_in::*;
use noise::{Fbm, NoiseFn};
use opengl_graphics::{GlGraphics, OpenGL};
use piston::input::RenderArgs;

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum Cell {
    Alive,
    JustDied,
    Dead,
}
impl Cell {
    pub fn get_bool_state(s: Self) -> bool {
        match s {
            Cell::Alive => true,
            Cell::JustDied => false,
            Cell::Dead => false,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum ScaleChange {
    ZoomIn(f32),
    ZoomOut(f32)
}

pub struct GameOfLife {
    pub arr: Vec<Vec<Cell>>,
    pub gl: OpenGL,
    pub conf: Config,
    bounds: (usize, usize, usize, usize),
    scale: f32
}

impl GameOfLife {
    pub fn new(gl: OpenGL, conf: Config) -> Self {
        let mut arr = vec![vec!(Cell::Dead; conf.width); conf.height];
        let noise = Fbm::new();

        for i in 0..conf.width {
            for j in 0..conf.height {
                arr[i as usize][j as usize] = match conf.start {
                    StartType::Blank => Cell::Dead,
                    StartType::Noise(c) => {
                        let scale: f64 = (conf.scale as f64) * 50.0; //
                        let val: f64 = noise.get([(i as f64) / scale, (j as f64) / scale]) * 100.0;
                        if val > c {
                            Cell::Alive
                        } else {
                            Cell::Dead
                        }
                    }
                };
            }
        }
        GameOfLife { arr, gl, conf, bounds: (0, conf.width, 0, conf.height), scale: 1.0 }
    }

    pub fn render(&mut self, args: &RenderArgs, lines: bool) {
        use graphics::*;

        #[allow(non_snake_case)]
        let JUST_DIED = tuple_to_arr(self.conf.just_died);
        #[allow(non_snake_case)]
        let DEAD = tuple_to_arr(self.conf.dead);
        #[allow(non_snake_case)]
        let ALIVE = tuple_to_arr(self.conf.alive);
        #[allow(non_snake_case)]
        let LINES = tuple_to_arr(self.conf.lines);


        let line_x = rectangle::rectangle_by_corners(
            0.0,
            0.0,
            (self.conf.width * self.conf.scale) as f64,
            2.0,
        );
        let line_y = rectangle::rectangle_by_corners(
            0.0,
            0.0,
            2.0,
            (self.conf.height * self.conf.scale) as f64,
        );
        let gr = self.arr.clone();

        let mult = (self.conf.width as f32 / (self.bounds.1 - self.bounds.0) as f32) as usize * self.conf.scale;
        let square = rectangle::square(0.0, 0.0, mult as f64);

        let mut glg = GlGraphics::new(self.gl);
        glg.draw(args.viewport(), |c, gl| {
            // Clear the screen.
            clear(DEAD, gl);

            if lines && self.conf.scale > 2 {
                for x in 0..self.conf.width {
                    let transform = c.transform.trans((x * mult) as f64, 0.0);
                    rectangle(LINES, line_y, transform, gl);
                }
                for y in 0..self.conf.height {
                    let transform = c.transform.trans(0.0, (y * mult) as f64);
                    rectangle(LINES, line_x, transform, gl);
                }
            }


            for bx in self.bounds.0..self.bounds.1 {
                for by in self.bounds.2..self.bounds.3 {
                    let x = bx - self.bounds.0;
                    let y = by - self.bounds.2;

                    let transform = c
                        .transform
                        .trans((x * mult) as f64, (y * mult) as f64);

                    match gr[bx as usize][by as usize] {
                        Cell::Alive => rectangle(ALIVE, square, transform, gl),
                        Cell::JustDied => rectangle(JUST_DIED, square, transform, gl),
                        Cell::Dead => {}
                    }
                }
            }
        });
    }

    pub fn update(&mut self) {
        let original = self.arr.clone();

        for x in 0..self.conf.width {
            for y in 0..self.conf.height {
                let mut no_of_neighbours = 0;

                for xo in (-1_i32)..=(1_i32) {
                    for yo in (-1_i32)..=(1_i32) {
                        let xxo = (x as i32) + xo;
                        let yyo = (y as i32) + yo;

                        if xo == 0 && yo == 0 {
                            continue;
                        } //check for current cell
                        if xxo < 0
                            || (xxo as usize) > self.conf.width - 1
                            || yyo < 0
                            || (yyo as usize) > self.conf.height - 1
                        {
                            continue;
                        } //check for overflow

                        if Cell::get_bool_state(original[xxo as usize][yyo as usize]) {
                            no_of_neighbours += 1;
                        }
                    }
                }

                self.arr[x][y] = self.conf.get_algo(no_of_neighbours, original[x][y]);
            }
        }
    }

    pub fn change(&mut self, px_xy: (usize, usize), new_state: Cell) {
        let mult = (self.conf.width as f32 / (self.bounds.1 - self.bounds.0) as f32) as usize * self.conf.scale;
        let bx = px_xy.0 / mult;
        let by = px_xy.1 / mult;

        let x = bx + self.bounds.0;
        let y = by + self.bounds.2;

        println!("{} / ({} - {}) = {}", self.conf.width, self.bounds.1, self.bounds.0, mult);
        // println!("{:?} -> ({}, {}) -> ({}, {}) using {}", px_xy, bx, by, x, y, mult);


        if x >= self.conf.width || y >= self.conf.height {
            return;
        }

        self.arr[x][y] = match new_state {
            Cell::Alive => Cell::Alive,
            Cell::JustDied => Cell::JustDied,
            Cell::Dead => {
                if self.arr[x][y] == Cell::Alive {
                    Cell::JustDied
                } else {
                    Cell::Dead
                }
            }
        };
    }
    pub(crate) fn change_unchecked (&mut self, xy: (usize, usize), new_state: Cell) {
        if xy.0 >= self.conf.width || xy.1 >= self.conf.height {
            return;
        }

        self.arr[xy.0][xy.1] = match new_state {
            Cell::Alive => Cell::Alive,
            Cell::JustDied => Cell::JustDied,
            Cell::Dead => {
                if self.arr[xy.0][xy.1] == Cell::Alive {
                    Cell::JustDied
                } else {
                    Cell::Dead
                }
            }
        };
    }


    pub fn clear(&mut self) {
        for x in 0..self.conf.width {
            for y in 0..self.conf.height {
                self.arr[x][y] = Cell::Dead;
            }
        }
    }

    pub fn change_scale (&mut self, sc: ScaleChange) {
        let old = self.bounds;
        let s = (self.conf.width + self.conf.height) as f32 / 2.0;

        match sc {
            ScaleChange::ZoomIn(f) => {
                let o: usize = (s * (f - 1.0)) as usize;

                let x1 = old.0 + o;
                let x2 = old.1 - o;
                let y1 = old.2 + o;
                let y2 = old.3 - o;

                if x1 >= x2 || y1 >= y2 {
                } else {
                    self.bounds = (x1, x2, y1, y2);
                    self.scale *= f;
                }
            }
            ScaleChange::ZoomOut(f) => {
                let o: usize = (s * (f - 1.0)) as usize;

                let x1 = old.0 - o;
                let x2 = old.1 + o;
                let y1 = old.2 - o;
                let y2 = old.3 + o;

                if (x1 >= x2 || y1 >= y2) || (x1 >= self.conf.width || y1 >= self.conf.height) {
                } else {
                    self.bounds = (x1, x2, y1, y2);
                    self.scale /= f;
                }
            }
        }

        println!("{}", self.scale);
    }

    pub fn translate (&mut self, x: i32, y: i32) {
        let x1 = (self.bounds.0 as i32) + x;
        let x2 = (self.bounds.1 as i32) + x;
        let y1 = (self.bounds.2 as i32) + y;
        let y2 = (self.bounds.3 as i32) + y;

        println!("({}, {}); ({}, {})", x1, x2, y1, y2);
        let eval =
            x1 >= x2 ||
                y1 >= y2 ||
                x1 < 0 ||
                y1 < 0 ||
                x2 > self.conf.width as i32 ||
                y2 > self.conf.height as i32;

        if eval {
            println!("how bout no");
        } else {
            self.bounds = (x1 as usize, x2 as usize, y1 as usize, y2 as usize);
        }
    }
}

pub fn tuple_to_arr<T>(t: (T, T, T, T)) -> [T; 4] {
    [t.0, t.1, t.2, t.3]
}
#[allow(dead_code)]
pub fn arr_to_tuple<T: Copy>(a: [T; 4]) -> (T, T, T, T) {
    (a[0], a[1], a[2], a[3])
}

#[cfg(test)]
mod gol_tests {
    use super::*;

    #[test]
    fn arr_to_tuple_test() {
        let t_arr = arr_to_tuple([true; 4]);
        let tup = (true, true, true, true);

        assert_eq!(t_arr, tup);
    }

    #[test]
    fn tuple_to_arr_test() {
        let a_tup = tuple_to_arr((0, 1, 2, 3));
        let arr = [0, 1, 2, 3];

        assert_eq!(a_tup, arr);
    }

    fn get_blank_grid() -> GameOfLife {
        GameOfLife::new(
            OpenGL::V3_2,
            Config::custom(
                StartType::Blank,
                2,
                Algorithm::Vanilla,
                true,
                false,
                (0.0, 0.0, 0.0, 1.0),
                (1.0, 1.0, 1.0, 1.0),
                (1.0, 0.0, 0.0, 1.0),
                (0.5, 0.5, 0.5, 1.0),
                (500, 500, 2),
            ),
        )
    }

    #[test]
    fn gol_default_test() {
        let gol = get_blank_grid();
        let a = gol.arr;
        let conf = gol.conf;

        for x in 0..conf.width {
            for y in 0..conf.height {
                assert!(!Cell::get_bool_state(a[x][y]));
                if Cell::get_bool_state(a[x][y]) {
                    println!("({}, {}) is true", x, y);
                }
            }
        }
    }

    #[test]
    fn gol_clear_test() {
        let mut gol = get_blank_grid();
        gol.clear();
        let a = gol.arr;
        let conf = gol.conf;

        for x in 0..conf.width {
            for y in 0..conf.height {
                assert!(!Cell::get_bool_state(a[x][y]))
            }
        }
    }

    #[test]
    fn gol_change_test() {
        let mut gol = get_blank_grid();
        gol.clear();
        gol.change_unchecked((10, 25), Cell::Alive);
        let a = gol.arr;
        let conf = gol.conf;

        for x in 0..conf.width {
            for y in 0..conf.height {
                let s = Cell::get_bool_state(a[x][y]);
                if x == 10 && y == 25 {
                    assert!(s);
                } else {
                    assert!(!s);
                }
            }
        }
    }
}
