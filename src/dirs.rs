use directories::ProjectDirs;
use file::put_text;
use std::fs::{create_dir_all, read_to_string};

pub const ORG: &str = "Maguire Testing";
pub const NAME: &str = "Conway Game Of Life";

#[derive(Copy, Clone, Debug)]
pub enum DirLocation {
    Config,
}

fn get_directory(ft: DirLocation) -> String {
    let backup = "assets".to_string();
    match ft {
        DirLocation::Config => match ProjectDirs::from("com", ORG, NAME) {
            None => backup,
            Some(ok) => match ok.config_dir().to_str() {
                Some(k) => k.to_string(),
                None => backup,
            },
        },
    }
}

pub const DEFAULT_CONFIG: &str = "ReadInData (
    start: \"Noise\",
    chance: Some(0.35),
    start_ups: 2,
    rules: Some(\"Vanilla\"),
    start_lines: true,
    start_running: false,
    alive: (0.0, 0.0, 0.0, 1.0),
    just_died: (1.0, 0.0, 0.0, 1.0),
    dead: (1.0, 1.0, 1.0, 1.0),
    lines: (0.5, 0.5, 0.5, 1.0),
    whs: (500, 500, 2),
)";

pub fn get_file_checked(ft: DirLocation, file: String) -> (Result<String, String>, String) {
    let dir = get_directory(ft);
    let path = format!("{}/{}", dir, file);
    let contents = read_to_string(path.clone());
    match contents {
        Ok(k) => (Ok(k), path),
        Err(_) => {
            match ft {
                DirLocation::Config => {
                    //matching for default
                    let p = path;
                    println!("Making conf file");

                    match create_dir_all(dir) {
                        Ok(_) => {
                            println!("Error finding conf file, making new one.");
                            if let Err(e) = put_text(p.clone(), DEFAULT_CONFIG) {
                                eprintln!("Error writing file: {}", e);
                            }
                            (Err(DEFAULT_CONFIG.to_string()), p)
                        }
                        Err(e) => {
                            println!("Error making dir: {}", e);
                            (Result::<String, String>::Err(DEFAULT_CONFIG.to_string()), p)
                        }
                    }
                }
            }
        }
    }
}

#[cfg(test)]
pub mod dirs_test {
    // use super::*;
    use rand::{thread_rng, Rng};
    // use std::fs::remove_file;

    pub fn get_bogus() -> String {
        format!("test-{}.ron", thread_rng().gen_range(0..10000))
    }

    // #[test]
    // fn file_checked_bad_in() {
    //     let bogus = get_bogus();
    //     let (res, p) = get_file_checked(DirLocation::Config, bogus.clone());
    //     println!("{:?}", res);
    //     assert!(res.is_err());
    //
    //     let str = match res {
    //         Ok(_) => "".to_string(), //The one scenario where Ok is an error
    //         Err(ok) => ok,
    //     };
    //     let str = str.as_str();
    //     assert_eq!(str, DEFAULT_CONFIG);
    //
    //     let (res, p2) = get_file_checked(DirLocation::Config, bogus);
    //     assert!(res.is_ok());
    //
    //     println!("{:?}", p2);
    //     remove_file_checked(p2);
    // }
    //
    // fn remove_file_checked(p: String) {
    //     match remove_file(p) {
    //         Ok(_) => println!("file successfully deleted"),
    //         Err(e) => println!("file not removed - {}", e),
    //     };
    // }
    //TODO: Fix this test, so it actually deletes the files
}
