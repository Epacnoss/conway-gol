use crate::gol::Cell;

pub fn vanilla(n: i32, s: Cell) -> Cell {
    let new_state;
    if !(2..=3).contains(&n) {
        new_state = false;
    } else if n == 3 {
        new_state = true;
    } else {
        new_state = Cell::get_bool_state(s);
    }

    state_to_cell(s, new_state)
}

//all conditions from here have been modified from
//https://github.com/benstockil/game-of-life-extended/blob/main/config.py
pub fn labyrinth(n: i32, s: Cell) -> Cell {
    let new_state;
    if n > 5 && n < 9 {
        new_state = true;
    } else if !(3..=8).contains(&n) {
        new_state = false;
    } else {
        new_state = Cell::get_bool_state(s);
    }

    state_to_cell(s, new_state)
}

pub fn corruptor(n: i32, s: Cell) -> Cell {
    let new_state;
    if n > 5 && n < 9 {
        new_state = true;
    } else if !(3..=8).contains(&n) {
        new_state = false;
    } else {
        new_state = Cell::get_bool_state(s);
    }
    state_to_cell(s, new_state)
}

//TODO: Test for this
fn state_to_cell(s: Cell, new_state: bool) -> Cell {
    match s {
        Cell::Alive => {
            if !new_state {
                Cell::JustDied
            } else {
                Cell::Alive
            }
        }
        Cell::JustDied => {
            if new_state {
                Cell::Alive
            } else {
                Cell::Dead
            }
        }
        Cell::Dead => {
            if new_state {
                Cell::Alive
            } else {
                Cell::Dead
            }
        }
    }
}

#[cfg(test)]
mod rules_tests {
    use super::*;

    pub fn algo_as_bool<F>(n: i32, b: bool, f: F) -> bool
    where
        F: FnOnce(i32, Cell) -> Cell,
    {
        let c;
        if b {
            c = Cell::Alive;
        } else {
            c = Cell::Dead;
        }

        let res = f(n, c);
        Cell::get_bool_state(res)
    }

    #[test]
    fn vanilla_test() {
        assert!(!algo_as_bool(1, false, vanilla));
        assert!(!algo_as_bool(1, true, vanilla));
        assert!(!algo_as_bool(2, false, vanilla));

        assert!(algo_as_bool(2, true, vanilla));
        assert!(algo_as_bool(3, true, vanilla));
        assert!(algo_as_bool(3, false, vanilla));

        for i in 4..8 {
            assert!(!algo_as_bool(i, true, vanilla));
            assert!(!algo_as_bool(i, false, vanilla));
        }
    }

    #[test]
    fn state_to_cell_test() {
        let s = state_to_cell;
        let a = Cell::Alive;
        let j = Cell::JustDied;
        let d = Cell::Dead;
        assert_eq!(a, s(a, true));
        assert_eq!(j, s(a, false));

        assert_eq!(a, s(j, true));
        assert_eq!(d, s(j, false));

        assert_eq!(a, s(d, true));
        assert_eq!(d, s(d, false));
    }
}
