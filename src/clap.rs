use crate::dirs::NAME;
use clap::{App, Arg, ArgMatches};

pub fn get_conf_file() -> String {
    let m = get_matches();
    String::from(m.value_of("config").unwrap_or("conf.ron"))
}

fn get_matches() -> ArgMatches {
    App::new(NAME)
        .version("0.5.0")
        .author("Jack Maguire <jackmaguire1234 at google mail dot com>")
        .about("Conway's Game Of Life")
        .arg(
            Arg::new("config")
                .short('c')
                .long("config")
                .takes_value(true)
                .help_heading(Some("Uses a custom config file other than conf.ron")),
        )
        .get_matches()
}
