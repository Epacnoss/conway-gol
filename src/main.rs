mod clap;
mod dirs;
mod gol;
mod read_in;
mod rule_sets;

use crate::clap::get_conf_file;
use crate::gol::*;
use crate::read_in::Config;
use glutin_window::GlutinWindow;
use opengl_graphics::OpenGL;
use piston::event_loop::*;
use piston::input::*;
use piston::window::{AdvancedWindow, WindowSettings};

fn main() {
    let ogl = OpenGL::V3_2;
    let conf = Config::new(get_conf_file());
    println!("{:?}", conf);

    let mut win: GlutinWindow = WindowSettings::new(
        "Conway's Game Of Life",
        [
            (conf.width * conf.scale) as u32,
            (conf.height * conf.scale) as u32,
        ],
    )
    .exit_on_esc(true)
    .opengl(ogl)
    .resizable(false)
    .build()
    .expect("error making window");

    let mut game = GameOfLife::new(ogl, conf);

    let mut ups = conf.start_ups;
    let mut running = conf.start_running;
    let mut lines = conf.start_lines;
    println!("{} ups", ups);

    let mut events = Events::new(EventSettings::new().ups(ups));
    let mut mouse_pos = [0.0, 0.0];
    let mut mouse_pressed = String::from("_");

    while let Some(e) = events.next(&mut win) {
        if let Some(r) = e.render_args() {
            game.render(&r, lines);
        }
        if e.update_args().is_some() && running {
            game.update();
        }

        e.mouse_cursor(|p1, p2| {
            mouse_pos = [p1, p2];
        });

        if let Some(Button::Mouse(btn)) = e.press_args() {
            let nu = format!("{:?}", btn);
            mouse_pressed = nu;
        }
        if let Some(Button::Mouse(_)) = e.release_args() {
            mouse_pressed = String::from("_");
        }

        if let Some(arg) = e.mouse_scroll_args() {
            let scroll = arg[1];
            let change = 1.1;
            if scroll > 0.0 {
                game.change_scale(ScaleChange::ZoomIn(change));
            } else if scroll < 0.0 {
                game.change_scale(ScaleChange::ZoomOut(change));
            }
        }

        if let Some(Button::Keyboard(key)) = e.press_args() {
            if key == Key::Space {
                //pause/play
                running = !running;
            } else if key == Key::C {
                //clear game screen
                game.clear();
            } else if key == Key::L {
                //toggle lines
                lines = !lines;
            } else if key == Key::LeftBracket {
                if ups >= 2 {
                    ups -= 1;
                    events.set_ups(ups);
                }
                println!("{}", ups);
            } else if key == Key::RightBracket {
                ups += 1;
                events.set_ups(ups);
                println!("{}", ups);
            }
            else if key == Key::Right {
                game.translate(1, 0);
            } else if key == Key::Left {
                game.translate(-1, 0);
            } else if key == Key::Up {
                game.translate(0, -1);
            } else if key == Key::Down {
                game.translate(0, 1);
            }

            win.set_title(format!("Conway's Game Of Life: {} UPS", ups));
        }



        if mouse_pressed.as_str() != "_" {
            let x = mouse_pos[0] as usize;
            let y = mouse_pos[1] as usize;
            let xy = (x, y);

            match mouse_pressed.as_str() {
                "Left" => game.change(xy, Cell::Alive),
                "Right" => game.change(xy, Cell::Dead),
                _ => {}
            };
        }
    }
}
