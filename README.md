# Conway's game of life
This is my personal implementation of [Conway's Game Of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life), written in rust.

# Colours
(By Default)
White = Dead,
Black = Alive,
Red = Died last generation

# Controls
\[ = slower
\] = faster
l = toggle showing lines
c = clear screen
Space = toggle pause/play

# Configuring
The Configuration file is written in [RON](https://github.com/ron-rs/ron) AKA Rusty Object Notation. The syntax is very similar to vanilla rust.
Line-by-line:
```rust
(
    start: "Noise", //This means that the starting pattern will be generated using Noise. Can also be "Blank"
    chance: Some(0.75), //This is for the noise. If you are using blank, leave this line out. A greater value means more chance of a cell being dead
    start_ups: 2, //The Updates per Second at the start
    rules: Some("Vanilla"), //The ruleset for Conway's Game Of Life. The other two values are "Corruptor" and "Labyrinth" - which are broken right now.
    start_lines: false, //Whether the grid should start showing lines
    start_running: false, //Whether the game should be started automatically when launched
    //then, for alive, just-dead, dead, and lines, these are colour values for the cells and lines in the format [R, G, B, A]. Make sure to add the decimal places, even for 1.0 and 0.0. Range between 1 and 0
```